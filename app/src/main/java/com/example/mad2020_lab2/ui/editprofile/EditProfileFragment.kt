package com.example.mad2020_lab2.ui.editprofile

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.mad2020_lab2.BuildConfig.APPLICATION_ID
import com.example.mad2020_lab2.R
import com.example.mad2020_lab2.User
import com.google.gson.Gson
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE
import com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class EditProfileFragment : Fragment(){

    companion object{
        const val INVOKE_GALLERY = 1
        const val INVOKE_CAMERA = 2
        const val MY_PERMISSIONS_REQUEST_STORAGE = 3
        const val MY_PERMISSIONS_REQUEST_CAMERA = 4
        private lateinit var selectedImage: Uri
        private var currentPhotoPath: String? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_edit_profile,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerForContextMenu(imageButton)

        if(arguments?.isEmpty == false) {
            fullName?.setText(arguments?.getString("${APPLICATION_ID}+FULL_NAME"))
            nickname?.setText(arguments?.getString("$APPLICATION_ID+NICKNAME"))
            emailAddress?.setText(arguments?.getString("$APPLICATION_ID+EMAIL_ADDRESS"))
            location?.setText(arguments?.getString("$APPLICATION_ID+LOCATION"))
            currentPhotoPath = arguments?.getString("$APPLICATION_ID+PHOTO")
            if (currentPhotoPath != null){
                profileIcon.setImageURI(Uri.parse(currentPhotoPath))
            }
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = activity?.menuInflater
        inflater?.inflate(R.menu.context_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.invokeGallery -> {
                if((ContextCompat.checkSelfPermission(view!!.context, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED))
                {
                    requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_STORAGE)
                }else
                {
                    invokeGallery()
                }
            }
            R.id.invokeCamera -> {
                if(!activity!!.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                    Toast.makeText(activity, "No Camera available", Toast.LENGTH_SHORT).show()
                }
                if((ContextCompat.checkSelfPermission(view!!.context, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(view!!.context, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED))
                {
                    requestPermissions( arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_CAMERA)
                }
                else
                {
                    invokeCamera()
                }
            }
            else ->
                Toast.makeText(activity, "Something wrong happened, sorry!", Toast.LENGTH_LONG).show()
        }
        return super.onContextItemSelected(item)
    }

    private fun invokeGallery(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, INVOKE_GALLERY)
    }

    private fun invokeCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(context!!.packageManager)?.also {
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    Toast.makeText(context, "An error occurred while creating the File", Toast.LENGTH_LONG).show()
                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        view!!.context,
                        "com.example.mad2020_lab2.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, INVOKE_CAMERA)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_STORAGE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    invokeGallery()
                }
                return
            }
            MY_PERMISSIONS_REQUEST_CAMERA -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    invokeCamera()
                }
                return
            }
            else -> {
            }
        }
    }


    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
        val storageDir = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM
            ), "Camera"
        )
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            currentPhotoPath = absolutePath
        }
    }

    private fun galleryAddPic(uri: Uri) {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            mediaScanIntent.data = uri
            this.activity?.sendBroadcast(mediaScanIntent)
        }
    }

    private fun launchImageCrop(uri: Uri){
        val imageFile = createImageFile()
        CropImage.activity(uri)
            .setGuidelines(CropImageView.Guidelines.ON)
            .setAspectRatio(1, 1)
            .setOutputUri(Uri.fromFile(File(imageFile.absolutePath)))
            .start(view!!.context, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == INVOKE_CAMERA && resultCode == RESULT_OK) {
            selectedImage = Uri.fromFile(File(
                currentPhotoPath
            ))
            launchImageCrop(selectedImage)
        }
        if (requestCode == INVOKE_GALLERY && resultCode == RESULT_OK){
            selectedImage = data!!.data
            currentPhotoPath = selectedImage.path
            launchImageCrop(selectedImage)
        }
        if(requestCode == CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            if(resultCode == RESULT_OK){
                val result = CropImage.getActivityResult(data)
                galleryAddPic(result.uri)
                profileIcon.setImageURI(result.uri)

            }
        }
        if(requestCode == CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
            Toast.makeText(view?.context, "Something went wrong, sorry!", Toast.LENGTH_LONG).show()
        }
    }

    override fun onPause() {
        super.onPause()
        if(currentPhotoPath == null) {
            val currentUser = User(
                fullName.text.toString(),
                nickname.text.toString(),
                emailAddress.text.toString(),
                location.text.toString())
            saveObjectToSharedPreference(view!!.context, "$APPLICATION_ID+USER_PREF", "key_user", currentUser)
        }
        else
        {
            val currentUser = User(
                fullName.text.toString(),
                nickname.text.toString(),
                emailAddress.text.toString(),
                location.text.toString(),
                currentPhotoPath)
            saveObjectToSharedPreference(view!!.context, "$APPLICATION_ID+USER_PREF", "key_user", currentUser)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (currentPhotoPath != null) {
            outState.putString("$APPLICATION_ID+PHOTO", currentPhotoPath)
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if(savedInstanceState != null){
            currentPhotoPath = savedInstanceState.getString("$APPLICATION_ID+PHOTO")
            if(currentPhotoPath != null){
                profileIcon.setImageURI(Uri.parse(currentPhotoPath))
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_edit_profile, menu)
        super.onCreateOptionsMenu(menu,inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.saveProfile) {
            saveProfile()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun saveProfile(){
        val bundle = Bundle()
        bundle.putString("$APPLICATION_ID+FULL_NAME", "${fullName.text}")
        bundle.putString("$APPLICATION_ID+NICKNAME", "${nickname.text}")
        bundle.putString("$APPLICATION_ID+EMAIL_ADDRESS", "${emailAddress.text}")
        bundle.putString("$APPLICATION_ID+LOCATION", "${location.text}")
        if(currentPhotoPath != "") {
            bundle.putString("$APPLICATION_ID+PHOTO", currentPhotoPath)
        }
        view!!.findNavController().navigate(R.id.action_nav_editprofile_to_nav_showprofile, bundle)
    }

    private fun saveObjectToSharedPreference(
        context: Context,
        preferenceFileName: String?,
        serializedObjectKey: String?,
        `object`: Any?
    ) {
        val sharedPreferences =
            activity!!.getSharedPreferences(preferenceFileName, 0)
        val sharedPreferencesEditor = sharedPreferences.edit()
        val gson = Gson()
        val serializedObject: String = gson.toJson(`object`)
        sharedPreferencesEditor.putString(serializedObjectKey, serializedObject)
        sharedPreferencesEditor.apply()
    }
}