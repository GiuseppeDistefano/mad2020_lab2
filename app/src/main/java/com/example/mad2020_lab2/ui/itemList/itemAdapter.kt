package com.example.mad2020_lab2.ui.itemList

import android.R.id
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.mad2020_lab2.BuildConfig.APPLICATION_ID
import com.example.mad2020_lab2.Item
import com.example.mad2020_lab2.R


class ItemAdapter(
    val items: MutableList<Item>,
    val itemKeys: MutableList<Int>,
    val parentFragment: ItemListFragment
):RecyclerView.Adapter<ItemAdapter.ViewHolder>(){

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v=LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(items[position],itemKeys[position], parentFragment)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        holder.unbind()
    }

    class ViewHolder(v:View): RecyclerView.ViewHolder(v){
        val title: TextView = v.findViewById(R.id.item_title)
        val price: TextView = v.findViewById(R.id.item_price)
        val editButton: Button = v.findViewById(R.id.editItem)
        val image: ImageView = v.findViewById(R.id.imagePic)
        val cardView: View = v.findViewById(R.id.cardView)


        fun bind(i: Item, k: Int, parentFragment: ItemListFragment){
            title.text = i.title
            price.text = i.price
            image.setImageURI(Uri.parse(i.photoItem))
            val bundle = Bundle()
            bundle.putInt("${APPLICATION_ID}+ITEM_ID", k)
            editButton.setOnClickListener { parentFragment.findNavController().navigate(R.id.action_nav_itemlist_to_nav_itemedit, bundle) }
            cardView.setOnClickListener{ parentFragment.findNavController().navigate(R.id.action_nav_itemlist_to_nav_itemdetails, bundle) }
        }

        fun unbind(){
           editButton.setOnClickListener(null)
           cardView.setOnClickListener(null)
        }
    }
}