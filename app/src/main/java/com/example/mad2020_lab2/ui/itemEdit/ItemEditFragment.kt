package com.example.mad2020_lab2.ui.itemEdit

import android.Manifest
import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.*
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.mad2020_lab2.BuildConfig.APPLICATION_ID
import com.example.mad2020_lab2.Item
import com.example.mad2020_lab2.R
import com.google.gson.Gson
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE
import com.theartofdev.edmodo.cropper.CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.fragment_item_edit.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class ItemEditFragment : Fragment() {

    companion object {
        const val INVOKE_GALLERY = 1
        const val INVOKE_CAMERA = 2
        const val MY_PERMISSIONS_REQUEST_STORAGE = 3
        const val MY_PERMISSIONS_REQUEST_CAMERA = 4
        private lateinit var selectedImage: Uri
        private var currentPhotoPath: String = ""
        private var categorySelectedIndex: Int = -1
        private var itemId: Int = -1
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AppCompatActivity?)?.setSupportActionBar(activity?.findViewById(R.id.toolbar))
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        registerForContextMenu(imageButton)

        super.onViewCreated(view, savedInstanceState)


        if(arguments?.isEmpty == false){
            itemId = arguments?.getInt("${APPLICATION_ID}+ITEM_ID")!!

            val currentItem = getSavedObjectFromPreference(view.context, "$APPLICATION_ID+SHARED_PREF", "$itemId", Item::class.java)
            if(currentItem?.category != null) {
                val currentCategory = currentItem.category
                categorySelectedIndex = resources.getStringArray(R.array.categories_array).indexOf(currentCategory)
            }
            if(currentItem?.title != null) {
                title.setText(currentItem.title)
            }
            if(currentItem?.price != null) {
                price.setText(currentItem.price)
            }
            if(currentItem?.expirationDate != null) {
                expirationDate.setText(currentItem.expirationDate)
            }
            if(currentItem?.location != null) {
                location.setText(currentItem.location)
            }
            if(currentItem?.description != null) {
                description.setText(currentItem.description)
            }
            if(currentItem?.photoItem != "" && currentItem?.photoItem != null) {
                imageView.setImageURI(Uri.parse(currentItem.photoItem))
                currentPhotoPath = currentItem.photoItem
            } //mettere qui un else per mettere in caso l'immagine di default?

        }
        else{
            val sharedPrefKeysInteger =
                context?.getSharedPreferences("$APPLICATION_ID+SHARED_PREF", Context.MODE_PRIVATE)
                    ?.all //gets every entry
                    ?.keys//gets only the keys
                    ?.map { it.toInt() }//maps the keys as integers
            if(sharedPrefKeysInteger?.isEmpty()!!) itemId = 0
            else {
                itemId = sharedPrefKeysInteger.sortedBy{ it }.last() +1 //gets last sorted element and adds 1
            }
            categorySelectedIndex = -1
            currentPhotoPath = ""

        }

        val stringArray = resources.getStringArray(R.array.categories_array)
        val categoriesPosition = arrayListOf<Int>()
        for(string in stringArray){
            if(string.startsWith("---")){
                categoriesPosition.add(stringArray.indexOf(string))
            }
        }

        val spinnerAdapter = object : ArrayAdapter<CharSequence?>(view.context, android.R.layout.simple_spinner_item, stringArray) {

            override fun isEnabled(position:Int):Boolean{
                return !categoriesPosition.contains(position)
            }

            override fun getDropDownView(
                position: Int, convertView: View?,
                parent: ViewGroup
            ): View {
                val myView = super.getDropDownView(position, convertView, parent)
                val myTextView = myView as TextView
                if(categoriesPosition.contains(position)){
                    if(position == 0)
                        myTextView.setTextColor(Color.GRAY)
                    else
                        myTextView.setTextColor(resources.getColor(R.color.colorPrimary, context.theme))
                }
                else{
                    myTextView.setTextColor(Color.BLACK)
                }

                return myView
            }
        }

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        categories_spinner.adapter = spinnerAdapter
        categories_spinner.setSelection(categorySelectedIndex)


        expirationDate.inputType = InputType.TYPE_NULL
        expirationDate.setOnClickListener {
            val calendar = Calendar.getInstance()
            val day = calendar[Calendar.DAY_OF_MONTH]
            val month = calendar[Calendar.MONTH]
            val year = calendar[Calendar.YEAR]
            val picker = DatePickerDialog(
                it.context,
                OnDateSetListener { _, year, monthOfYear, dayOfMonth -> expirationDate.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year) },
                year,
                month,
                day
            )
            picker.datePicker.minDate = System.currentTimeMillis()
            picker.show()
        }

        price.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                arg0: CharSequence?,
                arg1: Int,
                arg2: Int,
                arg3: Int
            ) {
            }

            override fun beforeTextChanged(
                arg0: CharSequence?,
                arg1: Int,
                arg2: Int,
                arg3: Int
            ) {
            }

            override fun afterTextChanged(arg0: Editable?) {
                val str: String = price.text.toString()
                if (str.isEmpty()) return
                val str2: String = perfectDecimal(str, 7, 2)
                if (str2 != str) {
                    price.setText(str2)
                    val pos: Int = price.text!!.length
                    price.setSelection(pos)
                }
            }

            private fun perfectDecimal(
                str: String,
                MAX_BEFORE_POINT: Int,
                MAX_DECIMAL: Int
            ): String {
                var str = str
                if (str[0] == '.') str = "0$str"
                val max = str.length
                var rFinal = ""
                var after = false
                var i = 0
                var up = 0
                var decimal = 0
                var t: Char
                while (i < max) {
                    t = str[i]
                    if (t != '.' && !after) {
                        up++
                        if (up > MAX_BEFORE_POINT) return rFinal
                    } else if (t == '.') {
                        after = true
                    } else {
                        decimal++
                        if (decimal > MAX_DECIMAL) return rFinal
                    }
                    rFinal += t
                    i++
                }
                return rFinal
            }
        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.fragment_item_edit_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if(id == R.id.saveItem){
            saveItem()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun saveItem(){
        val firstElementSpinner: String = resources.getStringArray(R.array.categories_array)[0]
        val selectedItemSpinner: String = categories_spinner.selectedItem.toString()
        if(selectedItemSpinner == firstElementSpinner || title.text.toString() == "" || price.text.toString() =="" || expirationDate.text.toString() == "" || location.text.toString() =="" || description.text.toString() == "" || currentPhotoPath == "")
            Toast.makeText(view?.context, "Some values are missing!", Toast.LENGTH_LONG).show()
        else {
            val bundle = Bundle()
            bundle.putInt("$APPLICATION_ID+ITEM_ID", itemId)

            val currentItem = Item(
                category = categories_spinner.selectedItem.toString(),
                title = title.text.toString(),
                price = price.text.toString(),
                expirationDate = expirationDate.text.toString(),
                location = location.text.toString(),
                description = description.text.toString(),
                photoItem = currentPhotoPath
            )
            saveObjectToSharedPreference(view!!.context, "$APPLICATION_ID+SHARED_PREF", "$itemId", currentItem)

           if(arguments?.isEmpty == false) {
                findNavController().navigate(
                    R.id.action_nav_itemedit_to_nav_itemdetails,
                    bundle
            )}
            else {

                findNavController().navigate(
                    R.id.action_nav_itemedit_to_nav_itemlist,
                    bundle
                )
            }
        }
    }


    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menu.setHeaderTitle(R.string.contextualMenuHeader)

        val inflater = activity?.menuInflater
        inflater?.inflate(R.menu.context_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.invokeGallery ->{
                if((ContextCompat.checkSelfPermission(view!!.context, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED))
                {
                    this.requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_STORAGE)

                }else
                {
                    invokeGallery()
                }
            }
            R.id.invokeCamera -> {
                if(!activity!!.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                    Toast.makeText(view?.context, "No Camera available", Toast.LENGTH_SHORT).show()
                }
                if((ContextCompat.checkSelfPermission(view!!.context, Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(view!!.context, Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED))
                {
                    this.requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_CAMERA)
                }
                else
                {
                    invokeCamera()
                }

            }
            else ->
                Toast.makeText(view?.context, "Something wrong happened, sorry!", Toast.LENGTH_LONG).show()
        }

        return super.onContextItemSelected(item)
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_STORAGE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    invokeGallery()
                }
                return
            }
            MY_PERMISSIONS_REQUEST_CAMERA-> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    invokeCamera()
                }
                return
            }
            else -> {
            }
        }
    }

    private fun invokeGallery(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent,
            INVOKE_GALLERY
        )
    }

    private fun invokeCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(context!!.packageManager)?.also {
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    Toast.makeText(view?.context, "An error occurred while creating the File", Toast.LENGTH_LONG).show()
                    null
                }

                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        view!!.context,
                        "com.example.mad2020_lab2.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent,
                        INVOKE_CAMERA
                    )
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
        val storageDir = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM
            ), "Camera"
        )
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            currentPhotoPath = absolutePath
        }
    }

    private fun galleryAddPic(uri: Uri) {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            mediaScanIntent.data = uri
            this.activity?.sendBroadcast(mediaScanIntent)
        }
    }

    private fun launchImageCrop(uri: Uri){
        val imageFile = createImageFile()
        CropImage.activity(uri)
            .setGuidelines(CropImageView.Guidelines.ON)
            .setOutputUri(Uri.fromFile(File(imageFile.absolutePath)))
            .start(view!!.context, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == INVOKE_CAMERA && resultCode == RESULT_OK) {
            selectedImage = Uri.fromFile(File(
                currentPhotoPath
            ))
            launchImageCrop(selectedImage)
        }
        if (requestCode == INVOKE_GALLERY && resultCode == RESULT_OK){
            selectedImage = data!!.data
            currentPhotoPath = selectedImage.path
            launchImageCrop(selectedImage)
        }
        if(requestCode == CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            if(resultCode == RESULT_OK){
                val result = CropImage.getActivityResult(data)
                galleryAddPic(result.uri)
                imageView.setImageURI(result.uri)

            }
        }
        if(requestCode == CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
            Toast.makeText(view?.context, "Something went wrong, sorry!", Toast.LENGTH_LONG).show()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("$APPLICATION_ID+ITEM_PHOTO", currentPhotoPath)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if(savedInstanceState != null){
            currentPhotoPath = savedInstanceState.getString("$APPLICATION_ID+ITEM_PHOTO")
            if(currentPhotoPath != ""){
                imageView.setImageURI(Uri.parse(currentPhotoPath))
            }
            else imageView.setImageResource(R.drawable.default_item)
        }
    }

    private fun saveObjectToSharedPreference(
        context: Context,
        preferenceFileName: String?,
        serializedObjectKey: String?,
        `object`: Any?
    ) {
        val sharedPreferences =
            activity!!.getSharedPreferences(preferenceFileName, 0)
        val sharedPreferencesEditor = sharedPreferences.edit()
        val gson = Gson()
        val serializedObject: String = gson.toJson(`object`)
        sharedPreferencesEditor.putString(serializedObjectKey, serializedObject)
        sharedPreferencesEditor.apply()
    }

    private fun <GenericClass> getSavedObjectFromPreference(
        context: Context,
        preferenceFileName: String?,
        preferenceKey: String?,
        classType: Class<GenericClass>?
    ): GenericClass? {
        val sharedPreferences =
            activity!!.getSharedPreferences(preferenceFileName, 0)
        if (sharedPreferences.contains(preferenceKey)) {
            val gson = Gson()
            return gson.fromJson(sharedPreferences.getString(preferenceKey, ""), classType)
        }
        return null
    }
}

