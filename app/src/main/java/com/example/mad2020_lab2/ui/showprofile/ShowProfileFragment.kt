package com.example.mad2020_lab2.ui.showprofile

import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.mad2020_lab2.BuildConfig.APPLICATION_ID
import com.example.mad2020_lab2.R
import com.example.mad2020_lab2.User
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_show_profile.*


class ShowProfileFragment : Fragment(){

    companion object{
        var actualPhoto: String? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_show_profile,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val currUser = getSavedObjectFromPreference(view.context, "$APPLICATION_ID+USER_PREF", "key_user",   User::class.java)
        if(currUser?.fullName != null) {
            fullName.text = currUser.fullName
        }
        if(currUser?.nickname != null) {
            nickname.text = currUser.nickname
        }
        if(currUser?.emailAddress != null) {
            emailAddress.text = currUser.emailAddress
        }
        if(currUser?.location != null) {
            location.text = currUser.location
        }
        if(currUser?.photo != "" && currUser?.photo != null) {
            profileIcon.setImageBitmap(BitmapFactory.decodeFile(currUser.photo))
            actualPhoto = currUser.photo
        }

        if(arguments?.isEmpty == false)
        {
            fullName.text = arguments?.getString("$APPLICATION_ID+FULL_NAME")
            nickname.text = arguments?.getString("$APPLICATION_ID+NICKNAME")
            emailAddress.text = arguments?.getString("$APPLICATION_ID+EMAIL_ADDRESS")
            location.text = arguments?.getString("$APPLICATION_ID+LOCATION")
            if(!arguments?.getString("$APPLICATION_ID+PHOTO").isNullOrEmpty())
            {
                actualPhoto = arguments?.getString("$APPLICATION_ID+PHOTO").toString()
                if(actualPhoto != null){
                    profileIcon.setImageURI(Uri.parse(actualPhoto))
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_show_profile, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here.
        val id = item.itemId
        if (id == R.id.createProfile) {
            editProfile()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun editProfile(){
        val b = Bundle()
        b.putString("$APPLICATION_ID+FULL_NAME", "${fullName.text}")
        b.putString("$APPLICATION_ID+NICKNAME", "${nickname.text}")
        b.putString("$APPLICATION_ID+EMAIL_ADDRESS", "${emailAddress.text}")
        b.putString("$APPLICATION_ID+LOCATION", "${location.text}")
        b.putString("$APPLICATION_ID+PHOTO", actualPhoto)

        view!!.findNavController().navigate(R.id.action_nav_showprofile_to_nav_editprofile, b)
    }


    private fun <GenericClass> getSavedObjectFromPreference(
        context: Context,
        preferenceFileName: String?,
        preferenceKey: String?,
        classType: Class<GenericClass>?
    ): GenericClass? {
        val sharedPreferences =
            activity!!.getSharedPreferences(preferenceFileName, 0)
        if (sharedPreferences.contains(preferenceKey)) {
            val gson = Gson()
            return gson.fromJson(sharedPreferences.getString(preferenceKey, ""), classType)
        }
        return null
    }
}