package com.example.mad2020_lab2.ui.itemdetails

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.mad2020_lab2.BuildConfig.APPLICATION_ID
import com.example.mad2020_lab2.R
import com.example.mad2020_lab2.Item
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_details_fragment.*


class ItemDetailsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.item_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val itemId = arguments?.getInt("${APPLICATION_ID}+ITEM_ID")!!

        val currentItem = getSavedObjectFromPreference(view.context, "$APPLICATION_ID+SHARED_PREF", "$itemId",
            Item::class.java )
        if(currentItem?.category != null) {
            itemCategory.text = currentItem.category
        }
        if(currentItem?.title != null) {
            itemTitle.text = currentItem.title
        }
        if(currentItem?.price != null) {
            itemPrice.text = currentItem.price
        }
        if(currentItem?.expirationDate != null) {
            itemExpiration.text = currentItem.expirationDate
        }
        if(currentItem?.location != null) {
            itemLocation.text = currentItem.location
        }
        if(currentItem?.description != null) {
            itemDescription.text = currentItem.description
        }
        if(currentItem?.photoItem != "" && currentItem?.photoItem != null) {
            itemPicture.setImageURI(Uri.parse(currentItem.photoItem))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_item_details, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.edit_item) {
            val b = Bundle()
            b.putInt("${APPLICATION_ID}+ITEM_ID",
                arguments?.getInt("${APPLICATION_ID}+ITEM_ID")!!)
            findNavController()
                .navigate(R.id.action_nav_itemdetails_to_nav_itemedit, b)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun <GenericClass> getSavedObjectFromPreference(
        context: Context,
        preferenceFileName: String?,
        preferenceKey: String?,
        classType: Class<GenericClass>?
    ): GenericClass? {
        val sharedPreferences =
            activity!!.getSharedPreferences(preferenceFileName, 0)
        if (sharedPreferences.contains(preferenceKey)) {
            val gson = Gson()
            return gson.fromJson(sharedPreferences.getString(preferenceKey, ""), classType)
        }
        return null
    }
}
