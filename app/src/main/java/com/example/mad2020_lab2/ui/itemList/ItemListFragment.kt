package com.example.mad2020_lab2.ui.itemList

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mad2020_lab2.BuildConfig.APPLICATION_ID
import com.example.mad2020_lab2.R
import com.example.mad2020_lab2.Item
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_item_list.*


class ItemListFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?{
        return inflater.inflate(R.layout.fragment_item_list, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPref = activity?.getSharedPreferences(
            "$APPLICATION_ID+SHARED_PREF", Context.MODE_PRIVATE)

        val items = mutableListOf<Item>()
        val itemKeys = mutableListOf<Int>()
        val keys: Map<String, *> = sharedPref?.all as Map<String, *>

        if(keys.isNotEmpty()) {
            for ((key, value) in keys) {

                val currentItem = getSavedObjectFromPreference(
                    view.context,
                    "$APPLICATION_ID+SHARED_PREF",
                    "$key",
                    Item::class.java)

                if (currentItem != null) {
                    items.add(currentItem)
                }
                itemKeys.add(key.toInt())
            }

            recycler_view.adapter = ItemAdapter(items,itemKeys, this)
            recycler_view.layoutManager = LinearLayoutManager(view.context)
        }
        else
        {
            val text:TextView = view.findViewById(R.id.message)
            text.visibility = View.VISIBLE
            text.text = getString(R.string.empty_list)
        }

        val fab: FloatingActionButton = view.findViewById(R.id.fab)
       fab.setOnClickListener {findNavController().navigate(R.id.action_nav_itemlist_to_nav_itemedit)}
    }


    private fun <GenericClass> getSavedObjectFromPreference(
        context: Context,
        preferenceFileName: String?,
        preferenceKey: String?,
        classType: Class<GenericClass>?
    ): GenericClass? {
        val sharedPreferences =
            activity!!.getSharedPreferences(preferenceFileName, 0)
        if (sharedPreferences.contains(preferenceKey)) {
            val gson = Gson()
            return gson.fromJson(sharedPreferences.getString(preferenceKey, ""), classType)
        }
        return null
    }
}
