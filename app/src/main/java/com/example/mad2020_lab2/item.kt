package com.example.mad2020_lab2

import com.google.gson.annotations.SerializedName

data class Item(@SerializedName("key_category") var category:String = "-",
                @SerializedName("key_title") var title:String = "-",
                @SerializedName("key_price") var price:String = "0.0",
                @SerializedName("key_expiration_date") var expirationDate:String = "00/00/0000",
                @SerializedName("key_location") var location:String = "Location, LO, 00000",
                @SerializedName("key_description") var description:String = "Lorem ipsum dolor sit amet",
                @SerializedName("key_photo_item") var photoItem:String =""
)