package com.example.mad2020_lab2

import com.google.gson.annotations.SerializedName

data class User(@SerializedName("key_fullname") var fullName:String? = "Fullname",
                @SerializedName("key_nickname") var nickname:String? = "Nickname",
                @SerializedName("key_emailaddress") var emailAddress:String? = "email@address.com",
                @SerializedName("key_location") var location:String? = "Location, LO, 00000",
                @SerializedName("key_photo") var photo:String? =""
)